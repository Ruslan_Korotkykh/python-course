import re

with open("classes_2_regexps_data_format-date.txt", 'r') as file:
  text = file.read()

pat = re.sub(r'([1-2][1-9]?[0-9]{2})/([0-3][0-9])/([0][0-9]|[1][0-2])', '\\2/\\3/\\1', text)

print(pat)
