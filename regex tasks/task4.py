import re

with open("classes_2_regexps_data_student_essays.txt", 'r') as file:
  text = file.read()
new = re.sub(r"(?i)\ba\b(\s\b[^aeyoiu])", "an\\1", text)
print(new)
