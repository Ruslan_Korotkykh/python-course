import re

with open("classes_2_regexps_data_student_essays.txt", 'r') as file:
  text = file.read()
mistakes = re.findall(r"(?i)(\d+|\d+\s?:-\d+)(p\.m|a\.m|am|pm)", text)
print(mistakes)
