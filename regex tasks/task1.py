import re

with open("classes_2_regexps_data_student_essays.txt", 'r') as file:
  text = file.read()
mistakes = re.findall(r"(?i)\ban\b[\s\n]+[^aeyoiu]\w+", text)
print(mistakes)
