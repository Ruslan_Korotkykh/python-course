import re

with open("classes_2_regexps_data_student_essays.txt", 'r') as file:
  text = file.read()
mistakes = re.findall(r"(?i)(can|could|shall|might|may|must|need|has to|have to)\s\bto\b", text)
print(mistakes)
