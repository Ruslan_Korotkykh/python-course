text = "To explore different supervised learning algorithms, we're going to use " \
       "an algorithms of small synthetic or artificial " \
       "datasets as examples, together with some larger real world datasets "
dictcreate ={textword:textword.upper() for textword in text.split()}
valuesort = list(dictcreate.values())
valuesort.sort(key=len, reverse=True)
for j in valuesort:
        print(j)

